# frozen_string_literal: true

RSpec.describe Rocali do
  it 'has a version number' do
    expect(Rocali::VERSION).not_to be nil
  end
end
