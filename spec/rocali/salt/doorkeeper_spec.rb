# frozen_string_literal: true

require 'rspec'

require 'rocali/salt/doorkeeper'
require 'rocali/exceptions/strategies/doorkeeper/not_api_secret_defined'

module Doorkeeper
  class Application
    def find_by(param)
      OpenStruct.new(name: 'cuco')
    end

    def where(param)
      param
    end

    def exists?
      true
    end
  end
end

class Target
  include Rocali::Salt::Doorkeeper
  attr_accessor :app_name

  def initialize(app_name = 'app_name')
    @app_name = app_name
  end
end

RSpec.describe Rocali::Salt::Doorkeeper do
  describe 'encoder_key' do
    context 'when rocali play_as server' do
      it 'return DOORKEEPER_API_SECRET environment value' do
        Rocali.play_as = :server
        ENV['DOORKEEPER_API_SECRET'] = 'DOORKEEPER_API_SECRET'
        expect(Target.new.encoder_key).to eq('DOORKEEPER_API_SECRET')
        ENV['DOORKEEPER_API_SECRET'] = nil
      end
    end

    context 'when rocali play_as authorizer' do
      it 'return the secret of the given app environment value' do
        Rocali.play_as = :authorizer
        allow(Doorkeeper::Application).to receive_message_chain(:find_by, :secret).
          with(name: 'app_name').with(no_args).and_return('DOORKEEPER_API_SECRET')
        expect(Target.new('app_name').encoder_key).to eq('DOORKEEPER_API_SECRET')
      end
    end
  end

  it 'raise' do
    Rocali.play_as = :server

    expect do
      Target.new.encoder_key
    end.to raise_exception(Rocali::Exceptions::Strategies::Doorkeeper::NotApiSecretDefined)
  end
end
