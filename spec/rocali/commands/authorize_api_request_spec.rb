# frozen_string_literal: true

require 'rocali/commands/strategies/doorkeeper/authorize_api_request'
require 'rocali/exceptions/strategies/doorkeeper/not_api_secret_defined'
require 'rocali/json_web_token/json_web_token'

RSpec.describe Rocali::Strategies::Doorkeeper::AuthorizeApiRequest do
  before do
    allow_any_instance_of(Rocali::Salt::Doorkeeper).to receive(:encoder_key).and_return('doorkeeper_api_secret')
  end

  let(:encoder_key) { 'doorkeeper_api_secret' }
  let(:valid_header) { {'Authorization' => 'Bearer token'} }
  let(:wrong_header) { {'Authorization' => 'Bearer wrong_token'} }
  let(:not_present_authorization_header) { {'noise_header' => 'header content'} }

  describe '.call' do
    context 'when decoded authorization header contain current app name' do
      it 'succeeds' do
        allow(Rocali::JsonWebToken).to receive(:decode).with(any_args).
          and_return({app_name: Rails.application.class.module_parent_name})
        expect(described_class.call(valid_header).result).to eq Rails.application.class.module_parent_name
      end
    end

    context 'when decoded authorization header contain current app name but do not match' do
      it 'return result nil' do
        allow(Rocali::JsonWebToken).to receive(:decode).with('wrong_token', encoder_key).
          and_return({app_name: 'different app'})
        expect(described_class.call(wrong_header).result).to be_nil
      end

      it 'return error invalid token' do
        allow(Rocali::JsonWebToken).to receive(:decode).with('wrong_token', encoder_key).
          and_return({app_name: 'different app'})
        expect(described_class.call(wrong_header).errors[:token]).to eq(['Invalid token'])
      end
    end

    context 'when authorization header is not present' do
      it 'return result nil' do
        expect(described_class.call(not_present_authorization_header).result).to be_nil
      end

      it 'return error missing token' do
        expect(described_class.call(not_present_authorization_header).errors[:token]).
          to eq(['Missing token'])
      end
    end
  end

  it 'user DOORKEEPER_API_KEY as encoder key' do
    expect(described_class.new('some_token').encoder_key).to eq 'doorkeeper_api_secret'
  end

  it 'user app_name as check param' do
    expect(described_class.new('some_token').check_param).to eq Rails.application.class.module_parent_name
  end
end
