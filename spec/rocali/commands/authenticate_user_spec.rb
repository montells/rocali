# frozen_string_literal: true

require 'rocali/commands/strategies/doorkeeper/authenticate_user'
require 'rocali/json_web_token/json_web_token'

module Doorkeeper
  class Application
    def where(param)
      param
    end

    def exists?
      true
    end
  end
end

RSpec.describe Rocali::Strategies::Doorkeeper::AuthenticateUser do
  before do
    allow_any_instance_of(Rocali::Salt::Doorkeeper).to receive(:encoder_key).and_return('doorkeeper_api_secret')
  end

  describe 'call' do
    it 'succeeds and return command object with result like JWK' do
      allow(Doorkeeper::Application).to receive_message_chain(:where, :exists?).
        with(name: 'app_name').with(no_args).and_return(true)
      allow(Rocali::JsonWebToken).to receive(:encode).with('app_name', 'doorkeeper_api_secret').and_return('jwt')
      expect(described_class.call('app_name').result).to eq 'jwt'
    end
  end

  it 'user DOORKEEPER_API_KEY as encoder key' do
    expect(described_class.new('app_name').encoder_key).to eq 'doorkeeper_api_secret'
  end

  context 'when app do not exists' do
    it 'returns errors "app_name does not exists"' do
      allow(Doorkeeper::Application).to receive_message_chain(:where, :exists?).
        with(name: 'wrong_app_name').with(no_args).and_return(nil)
      expect(described_class.call('wrong_app_name').errors[:app].first).to eq 'wrong_app_name does not exists'
    end

    it 'returns result nil' do
      allow(Doorkeeper::Application).to receive_message_chain(:where, :exists?).
        with(name: 'wrong_app_name').with(no_args).and_return(nil)
      expect(described_class.call('wrong_app_name').result).to be_nil
    end
  end
end
