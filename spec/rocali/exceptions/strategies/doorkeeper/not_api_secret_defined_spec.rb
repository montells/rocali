# frozen_string_literal: true

require 'rocali/exceptions/strategies/doorkeeper/not_api_secret_defined'

RSpec.describe Rocali::Exceptions::Strategies::Doorkeeper::NotApiSecretDefined do
  context 'when condition' do
    it 'succeeds' do
      expect(described_class.new.message).to eq 'DOORKEEPER_API_SECRET environment variable is not defined'
    end
  end
end
