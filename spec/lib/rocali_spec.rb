# frozen_string_literal: true

RSpec.describe Rocali do
  it 'is possible to provide config options' do
    described_class.config do |c|
      expect(c).to eq(described_class)
    end
  end

  describe 'parameters' do


    describe 'play_as' do
      it 'is possible to set play_as' do
        fake_class = class_double(described_class)
        expect(fake_class).to receive(:play_as=).with(:authorizer)
        fake_class.play_as = :authorizer
      end

      it 'is server as default' do
        expect(described_class.play_as).to eq :server
      end
    end
  end
end
