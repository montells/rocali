# frozen_string_literal: true

require 'jwt'
require 'rocali/json_web_token/json_web_token'

RSpec.describe Rocali::JsonWebToken do
  describe '.decode' do
    it 'calls JWT::Decode with token and encoder_key' do
      allow(JWT).to receive_message_chain(:decode, :[]).with('token', 'encoder_key').with(0).and_return({app_name: 'app_name'})
      expect(described_class.decode('token', 'encoder_key')[:app_name]).to eq 'app_name'
      expect(described_class.decode('token', 'encoder_key')['app_name']).to eq 'app_name'
    end
  end

  describe '.encode' do
    it 'calls JWT::Encode with payload and encoder_key' do
      payload = {key: 'value'}
      allow(JWT).to receive(:encode).with(payload, 'encoder_key').and_return('token')
      expect(described_class.encode(payload, 'encoder_key')).to eq 'token'
    end
  end
end
