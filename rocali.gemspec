# frozen_string_literal: true

require_relative 'lib/rocali/version'

Gem::Specification.new do |spec|
  spec.name          = 'rocali'
  spec.version       = Rocali::VERSION
  spec.authors       = ['Michel Sánchez Montells']
  spec.email         = ['montells@aleph.engineering']

  spec.summary       = 'Make easy authenticated dialogs between rails apps using JWT'
  spec.description   = 'Include gem, make some basic config, add before_action and enjoy JWT'
  spec.homepage      = 'https://gitlab.com/montells/rocali'
  spec.license       = 'MIT'
  spec.required_ruby_version = Gem::Requirement.new('>= 2.3.0')

  spec.metadata['homepage_uri'] = spec.homepage

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.extra_rdoc_files = ['README.md']
  spec.bindir        = 'bin'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_dependency 'activesupport', '>= 6.0.0', '< 7.0.0'
  spec.add_dependency 'jwt'
  spec.add_dependency 'simple_command'

  spec.add_development_dependency 'rails', '>= 6.0.0', '< 7.0.0'
  spec.add_development_dependency 'rspec', '~> 3.6'
  spec.add_development_dependency 'rubocop', '~> 0.60'
  spec.add_development_dependency 'rubocop-performance', '~> 1.5'
  spec.add_development_dependency 'rubocop-rspec', '~> 1.37'
  spec.add_development_dependency 'simplecov', '~> 0.16'
end
