require "rocali/version"

module Rocali
  class Error < StandardError; end

  class << self
    attr_writer :play_as

    def play_as
      @play_as || :server
    end

    def config
      yield self
    end
  end
end
