# frozen_string_literal: true

require 'active_support/hash_with_indifferent_access'

module Rocali
  class JsonWebToken
    class << self
      def encode(payload, encoder_key, exp = 24.hours.from_now)
        payload[:exp] = exp.to_i
        JWT.encode(payload, encoder_key)
      end

      def decode(token, encoder_key)
        $stdout.puts "token #{token}"
        $stdout.puts "encoder_key #{encoder_key}"
        body = JWT.decode(token, encoder_key)[0]
        HashWithIndifferentAccess.new body
      rescue StandardError => e
        $stdout.puts "error #{e.inspect}"
        nil
      end
    end
  end
end
