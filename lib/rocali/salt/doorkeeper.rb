# frozen_string_literal: true

module Rocali
  module Salt
    module Doorkeeper
      def encoder_key
        send("as_#{Rocali.play_as}")
      end

      private

      def as_authorizer
        ::Doorkeeper::Application.find_by(name: app_name).secret
      end

      def as_server
        raise Rocali::Exceptions::Strategies::Doorkeeper::NotApiSecretDefined unless ENV['DOORKEEPER_API_SECRET']

        ENV['DOORKEEPER_API_SECRET']
      end
    end
  end
end
