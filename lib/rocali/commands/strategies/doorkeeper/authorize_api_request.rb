# frozen_string_literal: true

require 'simple_command'
require 'rocali/salt/doorkeeper'
require 'rocali/json_web_token/json_web_token'
require 'rocali/exceptions/strategies/doorkeeper/not_api_secret_defined'

module Rocali
  module Strategies
    module Doorkeeper
      class AuthorizeApiRequest
        include Rocali::Salt::Doorkeeper
        prepend SimpleCommand

        attr_reader :check_param

        def initialize(headers)
          @headers = headers
          @check_param = Rails.application.class.module_parent_name.downcase
        end

        def call
          requester_app
        end

        private

        attr_reader :headers

        def requester_app
          dat = decoded_auth_token
          $stdout.puts "dat #{dat}"
          $stdout.puts "dat #{dat}"
          @requester_app ||= requester_app_name(dat[:app_name]) if dat
          @requester_app || (add_errors && nil)
        end

        def add_errors
          (errors.add(:token, 'Invalid token') if errors.empty?)
        end

        def requester_app_name(decoded_app_name)
          $stdout.puts "check_param == decoded_app_name #{check_param} == #{decoded_app_name} => #{check_param == decoded_app_name}"
          check_param if check_param == decoded_app_name
        end

        def decoded_auth_token
          @decoded_auth_token ||= Rocali::JsonWebToken.decode(http_auth_header, encoder_key)
        end

        def http_auth_header
          return headers['Authorization'].split.last if headers['Authorization'].present?

          errors.add(:token, 'Missing token')
          nil
        end
      end
    end
  end
end
