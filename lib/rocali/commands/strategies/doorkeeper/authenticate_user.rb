# frozen_string_literal: true

require 'simple_command'
require 'rocali/salt/doorkeeper'
require 'rocali/json_web_token/json_web_token'

module Rocali
  module Strategies
    module Doorkeeper
      class AuthenticateUser
        include Rocali::Salt::Doorkeeper
        prepend SimpleCommand

        def initialize(app_name)
          @app_name = app_name
        end

        def call
          return Rocali::JsonWebToken.encode(@app_name, encoder_key) if ::Doorkeeper::Application.
                                                                        where(name: @app_name).exists?

          errors.add(:app, "#{@app_name} does not exists")
        end
      end
    end
  end
end
