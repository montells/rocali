# frozen_string_literal: true

module Rocali
  module Exceptions
    module Strategies
      module Doorkeeper
        class NotApiSecretDefined < StandardError
          def message
            'DOORKEEPER_API_SECRET environment variable is not defined'
          end
        end
      end
    end
  end
end
